#include "pqueue.h"
#include <inttypes.h>
#include "malloc.h"
#include "thread.h"
#include <debug.h>
#include "threads/interrupt.h"

// TODO shit-ton of memory leaks

void pqueue_init(pqueue *pq) {
  pq->head = NULL;
  //pq->dead = NULL;
  lock_init(&pq->lock);
}

void pqueue_insert_locked(pqueue *pq, int64_t key, void *data) {
  ASSERT(!intr_context());
  lock_acquire(&pq->lock);
  pqueue_insert(pq, key, data);
  lock_release(&pq->lock);
}

void pqueue_insert(pqueue *pq, int64_t key, void *data) {
  if (!pq) return;
  pqueue_elem *e = malloc(sizeof(pqueue_elem));
  e->data = data;
  e->key = key;
  // If this needs to be first, set it as pq->head
  if (!pq->head || key < pq->head->key) {
    pqueue_elem *temp = pq->head;
    pq->head = e;
    e->next = temp;
  } else {
    pqueue_elem *walker = pq->head;
    while (walker->next && walker->next->key < key) {
      walker = walker->next;
    }
    e->next = walker->next;
    walker->next = e;
  }
}

void *pqueue_dequeue_conditional_locked(pqueue *pq, int64_t ticks) {
  ASSERT(!intr_context());
  lock_acquire(&pq->lock);
  void *result = pqueue_dequeue_conditional(pq, ticks);
  lock_release(&pq->lock);
  return result;
}

void *pqueue_dequeue_conditional(pqueue *pq, int64_t ticks) {
  if (pq->head != NULL && ticks >= pqueue_peek(pq)) {
    return pqueue_dequeue(pq);
  } else return NULL;
}

int64_t pqueue_peek(pqueue *pq) {
  return pq->head->key;
}

void *pqueue_dequeue(pqueue *pq) {
  if (!pq || !pq->head) return NULL;
  void *result = pq->head->data;
  // remove from the list
  // pqueue_elem *old_head = pq->head;TODO need this if we're gonig to free the head
  pq->head = pq->head->next;
  // add into the dead list
  /* TODO fix dead list implementation
  old_head->next = pq->dead;
  pq->dead = old_head;
  */
  return result;
}

/* TODO decide whether this is needed
void pqueue_destroy(pqueue *pq, bool free_data) {
  if (!pq) return;
  pqueue_elem *walker = pq->head;
  while (walker) {
    pqueue_elem *next = walker->next;
    if (free_data) free(walker->data);
    free(walker);
    walker = next;
  }
  free(pq);
}*/

/*
void pqueue_cleanup(pqueue *pq) {
  pqueue_elem *walker = pq->dead;
  while (walker) {
    pqueue_elem *next = walker->next;
    free(walker);
    walker = next;
  }
  pq->dead = NULL;
}
*/

// TODO delete
void pqueue_print_sleep_queue(int64_t t, pqueue *pq) {
  printf("Ticks: %"PRId64"\n", t);
  printf("Sleep queue:\n");
  pqueue_elem *walker = pq->head;
  int i = 0;
  while (walker) {
    printf("\tElement %d:\n", i);
    printf("\t\tKey: %" PRId64 "\n", walker->key);
    printf("\t\tThread name: %s\n", ((struct thread *)walker->data)->name);
    walker = walker->next;
    i++;
  }
}
