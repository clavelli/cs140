// TODO 
// comment
#include <stdbool.h>  // bool
#include <stddef.h>   // null
#include <stdint.h>   // int64_t
#include <stdio.h>    // printf
#include "threads/synch.h"    // lock

typedef struct pqueue_elem {
    void *data;
    int64_t key;
    struct pqueue_elem *next;
} pqueue_elem;

typedef struct {
  pqueue_elem *head;
  pqueue_elem *dead;
  struct lock lock;
} pqueue;

void pqueue_init(pqueue *);

void pqueue_insert_locked(pqueue *, int64_t, void *);
void pqueue_insert(pqueue *, int64_t, void *);

void *pqueue_dequeue_conditional(pqueue *, int64_t);
void *pqueue_dequeue_conditional_locked(pqueue *, int64_t);

// TODO this probably isn't needed in .h
int64_t pqueue_peek(pqueue *);

// TODO this probably isn't needed in .h
void *pqueue_dequeue(pqueue *);

// TODO do we need this?
void pqueue_destroy(pqueue *pq, bool free_data/*TODO do we need tihs?*/);

// TODO delete
void pqueue_print_sleep_queue(int64_t t, pqueue *pq);
