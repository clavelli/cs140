#ifndef FIXED_H
#define FIXED_H

#define DENOM 0x00004000

// fixed point arithmetic in 17.14 format

typedef int fixed;
// conversion
fixed int_to_fixed(int);
fixed fixed_to_int(fixed);
fixed fixed_create(int num, int den);

fixed fixed_add(fixed, fixed);
fixed fixed_subtract(fixed, fixed);
fixed fixed_multiply(fixed, fixed);
fixed fixed_divide(fixed, fixed);

fixed fixed_add_int(fixed, int);
fixed fixed_subtract_int(fixed, int);
fixed fixed_multiply_int(fixed, int);
fixed fixed_divide_int(fixed, int);

#endif /* /lib/kernel/fixed.h */
