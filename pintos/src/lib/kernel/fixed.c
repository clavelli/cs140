#include "lib/kernel/fixed.h"
#include <inttypes.h>

typedef int64_t fixed64_t;

fixed int_to_fixed(int i) {
  return i * DENOM;
}

fixed fixed_to_int(fixed f) {
  // double check this
  if (f > 0) {
    return (f + DENOM/2) / DENOM;
  } else {
    return (f + DENOM/2) / DENOM;
  }
}

fixed fixed_create(int num, int den) {
  fixed f = int_to_fixed(num);
  return fixed_divide(f, den);
}

// We could just tell the user to use + and -, but this maintains the 
// abstraction and keeps things consistent
fixed fixed_add(fixed f1, fixed f2) {
  return f1 + f2;
}

fixed fixed_subtract(fixed f1, fixed f2) {
  return f1 - f2;
}

fixed fixed_multiply(fixed f1, fixed f2) {
  return (((fixed64_t) f1) * f2) / DENOM;
}

fixed fixed_divide(fixed f1, fixed f2) {
  return (((fixed64_t) f1) * DENOM) / f2;
}

fixed fixed_add_int(fixed f, int i) {
  return fixed_add(f, int_to_fixed(i));
}

fixed fixed_subtract_int(fixed f, int i) {
  return fixed_subtract(f, int_to_fixed(i));
}

fixed fixed_multiply_int(fixed f, int i) {
  return f * i;
}

fixed fixed_divide_int(fixed f, int i) {
  return f / i;
}
